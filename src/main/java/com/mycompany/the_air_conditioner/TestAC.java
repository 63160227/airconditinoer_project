/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.the_air_conditioner;


/**
 *
 * @author a
 */
public class TestAC {

    public static void main(String[] args) {
        AC ac1 = new AC(2, false);
        AC ac2 = new AC(18,false);
        AC ac3 = new AC(20,false);
        
        remote re1 = new remote(ac1);
        remote re2 = new remote(ac2);
        remote re3 = new remote(ac3);

        re1.isTurnOn();
        re1.turnOn();
        re1.isTurnOn();
        re1.showTemp();
        
  
        re1.increaseTemp();
        re1.turnOn();
        re1.isTurnOn();
        re1.showTemp();
        
        re1.decreaseTemp();
        re1.turnOn();
        re1.isTurnOn();
        re1.showTemp();
        System.out.println("____________________");
        
        re2.isTurnOn();
        re2.turnOn();
        re2.isTurnOn();
        re2.showTemp();
        
        re2.increaseTemp();
        re2.turnOn();
        re2.isTurnOn();
        re2.showTemp();
        
        re2.decreaseTemp();
        re2.turnOn();
        re2.isTurnOn();
        re2.showTemp();
        System.out.println("____________________");
         
        re3.isTurnOn();
        re3.turnOn();
        re3.isTurnOn();
        re3.showTemp();
        
        re3.increaseTemp();
        re3.turnOn();
        re3.isTurnOn();
        re3.showTemp();
        
        re3.decreaseTemp();
        re3.turnOn();
        re3.isTurnOn();
        re3.showTemp();
        System.out.println("____________________");
        
        
    }
}
