/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.the_air_conditioner;

/**
 *
 * @author a
 */
public class remote {

    AC air = null;

    public remote() {
    }

    public remote(AC air) {
        this.air = air;
    }

    public void connectAirCondition(AC air1) {
        this.air = air1;
    }

    public boolean isConnected() {
        if (this.air == null) {
            System.out.println("Air condition not connected with remote");
            return false;
        } else {
            return true;
        }

    }

    public void isTurnOn() {
        if (this.isConnected()) {
            if (this.air.isStatus()) {
                System.out.println("Air condition is turn on");
            } else {
                System.out.println("Air condition is turn off");
            }
        }
    }

    public void turnOn() {
        if (this.isConnected()) {
            this.air.turnOn();
        }
    }

    public void increaseTemp() {
        if (this.air != null) {
            this.air.tempAC++;
        }
    }

    public void decreaseTemp() {
        if (this.air != null) {
            this.air.tempAC--;
        }
    }

    public void showTemp() {
        System.out.println("Current temperature : " + this.air.getTemp() + "c");
    }
}
