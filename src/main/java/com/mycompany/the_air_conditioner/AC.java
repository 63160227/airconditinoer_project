/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.the_air_conditioner;

/**
 *
 * @author a
 */
public class AC {

    int tempAC = 0;
	boolean status = false;
	
	public AC (int tempAC, boolean status) {
		this.tempAC = tempAC;
		this.status = status;
	}

	public int getTemp() {
		return tempAC;
	}

	public void increaseTemp() {
		this.tempAC++;
	}
	
	public void decreaseTemp() {
		this.tempAC--;
	}

	public boolean isStatus() {
		return status;
	}
	
	public void turnOn() {
		this.status = true;
	}
	
        public void turnOff() {
		this.status = false;
	}
	
}
        